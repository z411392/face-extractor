## 說明
這個專案會產生一個 binary excutable。
這個 binary excutable 可以辨識照片中的人臉將它們個別儲存，並額外儲存他們的特徵向量。

### colab 版
https://colab.research.google.com/drive/17Q2JTwWAvWtX3BzoH0Pto-4mbMrikTAH?usp=sharing

## 注意事項

### 在 Mac M1 上安裝 numpy
繼續用舊版 Python 裝 numpy 可能會遇到問題，建議先將 Python 升到 3.11（一開始不明所以很久結果升上去就好了😂）。

### Library not loaded: libmbedcrypto
參考[這篇](https://stackoverflow.com/questions/77110765/error-while-run-command-ffmpeg-library-not-loaded-opt-homebrew-opt-mbedtls-l)。

重新安裝 ffmpeg 就可以：
```bash
brew uninstall librist --ignore-dependencies
brew uninstall mbedtls --ignore-dependencies
brew reinstall ffmpeg
```

### 設定 hidden-import 及 add-data
參考[這篇](https://github.com/ets-labs/python-dependency-injector/issues/438#issuecomment-1411692470)。

```Makefile
build:
	@pyinstaller -F ./main.py \
		--hidden-import=configparser \
		--hidden-import=dependency_injector.errors \
		--hidden-import=yaml \
		--hidden-import=six \
		--hidden-import=dependency_injector.wiring \
		--add-data=$(MODEL_PATH)/face_recognition_models:face_recognition_models
```

### 下載 dlib 需要的權重
https://github.com/serengil/deepface/issues/847#issuecomment-1754641523

```bash
wget http://dlib.net/files/shape_predictor_5_face_landmarks.dat.bz2 -P ~/Desktop
```
解壓縮並放到 `~/.deepface/weights/`。

## 使用方法
### 編譯
```bash
make build
```

### 執行
```bash
./dist/main 圖片網址
```