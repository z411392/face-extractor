import asyncio
import traceback
import logging
from dotenv import load_dotenv
from src.startup import startup
import argparse
from multiprocessing import freeze_support
from typing import List
from typing import Optional
from src.infrastructure import ImageDownloadService, FaceExtractService
import os
import shutil

async def main(URLs: List[str], /, force: bool, uid: Optional[str]):
    async with startup() as container:
        imageDownloadService:ImageDownloadService = await container.imageDownloadService()
        faceExtractService:FaceExtractService = await container.faceExtractService()
        if not args.URLs or len(args.URLs) == 0: raise Exception("必須輸入圖片網址")
        for URL in URLs:
            try:
                pHash = await imageDownloadService.downloadImage(URL, force)
                if not pHash: continue
                async for id in faceExtractService.extractFaces(pHash):
                    if not uid: continue
                    if not os.path.exists(f'{os.getcwd()}/ownerships'): os.makedirs(f'{os.getcwd()}/ownerships')
                    shutil.copyfile(f'{os.getcwd()}/faces/{id}.jpeg', f'{os.getcwd()}/ownerships/{uid}--{id}.jpeg')
            except Exception as exception: logging.error(exception)

if __name__ == "__main__":
    freeze_support()  # https://stackoverflow.com/questions/46335842/python-multiprocessing-throws-error-with-argparse-and-pyinstaller
    load_dotenv()
    logging.getLogger().setLevel(logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument("URLs", type=str, nargs="*")
    parser.add_argument("-u", "--uid", type=str, required=False)
    parser.add_argument("-f", "--force", action=argparse.BooleanOptionalAction)
    args = parser.parse_args()
    if not args.URLs or len(args.URLs) == 0: raise Exception("必須輸入圖片網址")
    loop = asyncio.get_event_loop()
    try: loop.run_until_complete(main(args.URLs, force=args.force, uid=args.uid))
    except KeyboardInterrupt as exception: pass
    except Exception as exception: print(traceback.format_exc())
