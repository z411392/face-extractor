include .env
export

MODEL_PATH := $(shell python -c 'import site; print(site.getsitepackages()[0])')
.PHONY: clean build rebuild run

clean:
	@rm -rf build

build:
	@pyinstaller -F ./main.py \
		--hidden-import=configparser \
		--hidden-import=dependency_injector.errors \
		--hidden-import=yaml \
		--hidden-import=six \
		--hidden-import=dependency_injector.wiring \
		--add-data=$(MODEL_PATH)/face_recognition_models:face_recognition_models\
		--add-data=./planes.pickle:planes.pickle

rebuild:
	$(MAKE) clean
	$(MAKE) build

run:
	@python main.py https://akamai.gethornet.com/uploads/photos/3n24g/thumbnail_retina_caff4a28-d083-40ce-9d50-809377a29da6.jpg -f