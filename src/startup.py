from contextlib import asynccontextmanager
from .Container import Container

@asynccontextmanager
async def startup():
  try:
    container = Container()
    container.config.processPoolExecutors.from_env('PROCESS_POOL_EXECUTORS', as_=int, default=3)
    container.wire(modules=[__name__])
    await container.init_resources()
    yield container
  finally:
    await container.shutdown_resources()