from concurrent.futures import ProcessPoolExecutor
from .LSH import LSH
import os
from asyncio import get_running_loop
from .utils import saveImage, loadImage, locationsOf, vectorsOf
from aiofile import async_open
import json
import logging
class FaceExtractService:
    processPool: ProcessPoolExecutor
    lsh: LSH
    savedTo: str
    def __init__(self, processPool: ProcessPoolExecutor, lsh: LSH):
        self.processPool = processPool
        self.lsh = lsh
        self.savedTo = os.getcwd()
    def idFor(self, pHash:str, lsh:str): return f"{pHash[:8]}-{pHash[8:12]}-{pHash[12:]}-{lsh[:4]}-{lsh[4:]}"        
    async def extractFaces(self, pHash:str):
        if not os.path.exists(f"{self.savedTo}/faces"): os.makedirs(f"{self.savedTo}/faces")
        if not os.path.exists(f"{self.savedTo}/vectors"): os.makedirs(f"{self.savedTo}/vectors")
        logging.info(f'正在抽取 {pHash}')
        image = loadImage(f'{self.savedTo}/images/{pHash}.jpeg')
        locations = locationsOf(image)
        vectors = vectorsOf(image, locations)
        for index, (top, right, bottom, left) in enumerate(locations):
            face = image[top:bottom, left:right, ::-1]
            vector = vectors[index].tolist()
            id = self.idFor(pHash, await self.lshOf(vector))
            await self.saveImage(f'{self.savedTo}/faces/{id}.jpeg', face)
            async with async_open(f"{self.savedTo}/vectors/{id}.json", "w+") as file: await file.write(json.dumps(vector))
            yield id
        logging.info(f'已抽取 {pHash}')
    def saveImage(self, savedTo, image): return get_running_loop().run_in_executor(self.processPool, saveImage, savedTo, image)
    def lshOf(self, vector): return get_running_loop().run_in_executor(self.processPool, self.lsh, vector)