from hashlib import md5 as MD5
import cv2
import numpy as np
from face_recognition import load_image_file, face_locations, face_encodings

def md5(buffered: bytes):
    hashed = MD5()
    hashed.update(buffered)
    return hashed.hexdigest()
def pHash(image): return cv2.img_hash.pHash(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)).tobytes().hex()
def imageFromBuffer(buffered: bytes): return cv2.imdecode(np.frombuffer(buffered, dtype=np.uint8), -1)
def saveImage(savedTo: str, image):
    success, buffer = cv2.imencode(".jpg", image)
    if success: buffer.tofile(savedTo)
def loadImage(imagePath: str): return load_image_file(imagePath)
def locationsOf(image): return face_locations(image)
def vectorsOf(image, locations): return face_encodings(image, locations)