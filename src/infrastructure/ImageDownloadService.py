from aiohttp import ClientSession
from concurrent.futures import ProcessPoolExecutor
import os
from aiofile import async_open
from glob import glob
import logging
from asyncio import get_running_loop
from .utils import pHash, md5, imageFromBuffer, saveImage

class ImageDownloadService:
    httpClient: ClientSession
    processPool: ProcessPoolExecutor
    savedTo: str
    def __init__(self, httpClient: ClientSession, processPool: ProcessPoolExecutor):
        self.httpClient = httpClient
        self.processPool = processPool
        self.savedTo = os.getcwd()

    async def downloadImage(self, URL: str, force:bool):
        md5 = await self.md5(URL.encode("utf-8"))
        if not os.path.exists(f"{self.savedTo}/hashes"): os.makedirs(f"{self.savedTo}/hashes")
        found = glob(f"{self.savedTo}/hashes/*/{md5}")
        if not force and len(found): return logging.info(f"忽略 {URL}")
        pHash = None
        try:
            logging.info(f"正在下載 {URL}")
            async with self.httpClient.get(URL) as response:
                image = await self.imageFromBuffer(await response.content.read())
                pHash = await self.pHash(image)
                if not os.path.exists(f"{self.savedTo}/hashes/{pHash}"): os.makedirs(f"{self.savedTo}/hashes/{pHash}")
                if not os.path.exists(f"{self.savedTo}/images"): os.makedirs(f"{self.savedTo}/images")
                await self.saveImage(f"{self.savedTo}/images/{pHash}.jpeg", image)
                logging.info(f"已下載 {URL}")
                return pHash
        except Exception as error: return logging.error(error)
        finally:
            if not pHash: return
            async with async_open(f"{self.savedTo}/hashes/{pHash}/{md5}", "w+") as file: await file.write(URL)

    def pHash(self, image): return get_running_loop().run_in_executor(self.processPool, pHash, image)
    def md5(self, buffered: str): return get_running_loop().run_in_executor(self.processPool, md5, buffered)
    def imageFromBuffer(self, buffered: str): return get_running_loop().run_in_executor(self.processPool, imageFromBuffer, buffered)
    def saveImage(self, savedTo, image): return get_running_loop().run_in_executor(self.processPool, saveImage, savedTo, image)
