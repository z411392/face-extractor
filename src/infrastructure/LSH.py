from bitarray import bitarray
from bitarray.util import ba2hex
import numpy as np
from numpy.random import random_sample
import pickle
import os

class LSH:
  def __init__(self, feat_dim = 128, sig_dim = 64, b = 16):
    self.feat_dim = feat_dim
    self.sig_dim = sig_dim
    self.b = b
    self.planes = self._createPlanes(sig_dim, feat_dim)
  def _createPlanes(self, sig_dim, feat_dim):
    savedTo = f'{os.getcwd()}/planes.pickle'
    planes = None
    if os.path.exists(savedTo):
      with open(savedTo, 'rb') as file: planes = pickle.load(file)
    if planes is None:
      planes = random_sample((sig_dim, feat_dim))
      with open(savedTo, 'wb') as file:
        pickle.dump(planes, file)
    return planes
  def __call__(self, feature):
    '''
    https://github.com/spininertia/pylsh/blob/master/lsh.py
    '''
    r = self.sig_dim // self.b
    signature = []
    X = np.array(list(feature))
    vector = (X - X.mean())/(X.std())
    for value in np.dot(self.planes, vector): signature.append(1 if value > 0 else 0)
    signature = bitarray(signature)
    result = []
    for index in range(self.b):
      start = r * index
      end = r * (index + 1)
      result.append(ba2hex(signature[start:end]))
    return ''.join(result)