from aiohttp import ClientSession
from dependency_injector.containers import DeclarativeContainer
from concurrent.futures import ProcessPoolExecutor
from dependency_injector import providers
from .infrastructure.ImageDownloadService import ImageDownloadService
from .infrastructure.FaceExtractService import FaceExtractService
from .infrastructure.LSH import LSH

async def initHttpClient():
  httpClient = ClientSession()
  yield httpClient
  await httpClient.close()

async def initProcessPool(executors:int):
  processPool = ProcessPoolExecutor(executors)
  yield processPool
  processPool.shutdown()

async def initLSH():
  lsh = LSH()
  yield lsh

class Container(DeclarativeContainer):
  config = providers.Configuration()
  httpClient = providers.Resource(initHttpClient)
  processPool = providers.Resource(initProcessPool, config.processPoolExecutors)
  lsh = providers.Resource(initLSH)
  imageDownloadService = providers.Factory(ImageDownloadService, httpClient=httpClient, processPool=processPool)
  faceExtractService = providers.Factory(FaceExtractService, processPool=processPool, lsh=lsh)